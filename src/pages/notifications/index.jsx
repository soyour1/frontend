import { Helmet } from 'react-helmet';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getNotifications } from '@redux/reducers/notification';
import { BackNavigation, Box, NotificationsItem, Empty } from '@components';
import { Skeleton, Pagination } from 'antd';

function Notifications() {
  const dispatch = useDispatch();
  const [page, setPage] = useState(1); // eslint-disable-line
  const {
    data: notifications,
    loading: notificationsLoading,
    pagination: notificationsPagination
  } = useSelector((state) => state.notification.notifications);
  useEffect(() => {
    dispatch(getNotifications({ limit: 5, page: 1 }));
  }, [dispatch]); // eslint-disable-line

  const changePage = (pageID) => {
    setPage(pageID);
    dispatch(
      getNotifications({
        page: pageID
      })
    );
  };

  console.log(notificationsPagination);

  return (
    <>
      <Helmet>
        <title>Notifikasi</title>
      </Helmet>

      <Box className="product container">
        <BackNavigation className="profile-back" />
        <Box>
          {notificationsLoading &&
            Array(3)
              .fill(1)
              .map((i) => <Skeleton.Button key={i} className="skeleton-notification" active />)}

          {!notificationsLoading && notifications?.length < 1 && (
            <Box>
              <Empty className="my-4 mx-auto" description="Belum ada notifikasi" />
            </Box>
          )}

          {!notificationsLoading &&
            notifications?.length > 0 &&
            notifications?.map((item, i) => <NotificationsItem key={i} notification={item} />)}

          {!notificationsLoading &&
            notificationsPagination &&
            notificationsPagination?.page_count > 1 && (
              <Box className="pt-10">
                <Pagination
                  defaultCurrent={notificationsPagination?.page}
                  defaultPageSize={notificationsPagination?.page_size}
                  total={notificationsPagination?.count}
                  onChange={(page) => {
                    changePage(page);
                  }}
                />
              </Box>
            )}
        </Box>
      </Box>
    </>
  );
}

export default Notifications;
