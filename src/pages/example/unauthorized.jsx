import { Helmet } from 'react-helmet';
import { Footer, Unauthorized } from '@components';

function Example() {
  return (
    <>
      <Helmet>
        <title>Example</title>
      </Helmet>
      <Unauthorized description="Anda tidak memiliki akses" />
    </>
  );
}

export default Example;
