import { Helmet } from 'react-helmet';
import { useEffect } from 'react';
import { Typography } from 'antd';
import { Link } from 'react-router-dom';
import { Button, Form, Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@components';
import { resend } from '@redux/reducers/auth';

const { Paragraph, Title } = Typography;

function ResendVerification() {
  const { success, loading } = useSelector((state) => state.auth.resend);
  const dispatch = useDispatch();

  const onFinish = async (values) => {
    dispatch(resend(values));
  };

  const [form] = Form.useForm();

  useEffect(() => {
    if (success === true) {
      form.resetFields();
    }
  }, [success]); // eslint-disable-line

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      <Box className="layout-auth-form-wrapper">
        <Title className="layout-auth-title" level={3}>
          Resend Email Verification
        </Title>
        <Form
          form={form}
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            name="email"
            label="Email"
            rules={[
              {
                required: true,
                message: 'Silakan masukkan email Anda'
              },
              { type: 'email', message: 'Email tidak valid' }
            ]}
          >
            <Input placeholder="Contoh: johndee@gmail.com" />
          </Form.Item>

          <Form.Item>
            <Button
              loading={loading}
              disabled={loading}
              htmlType="submit"
              type="primary"
              block
              size="large"
            >
              Resend
            </Button>
          </Form.Item>
        </Form>
      </Box>
      <Box className="layout-auth-info">
        <Paragraph>
          Belum punya akun?{' '}
          <Link to="/register">
            <strong>Daftar di sini</strong>
          </Link>
        </Paragraph>
      </Box>

      <Helmet>
        <title>Resend Verification</title>
      </Helmet>
    </>
  );
}

export default ResendVerification;
