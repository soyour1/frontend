import { Helmet } from 'react-helmet';
import { useEffect } from 'react';
import { Typography } from 'antd';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, Input } from 'antd';
import { Box } from '@components';
import { register } from '@redux/reducers/auth';

const { Paragraph, Title } = Typography;

function Register() {
  const { success, loading } = useSelector((state) => state.auth.register);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onFinish = async (values) => {
    dispatch(register(values));
  };

  const [form] = Form.useForm();

  useEffect(() => {
    if (success === true) {
      form.resetFields();
      navigate('/activate');
    }
  }, [success]); // eslint-disable-line

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      <Box className="layout-auth-form-wrapper">
        <Title className="layout-auth-title" level={3}>
          Daftar
        </Title>
        <Form
          form={form}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            name="name"
            label="Nama"
            rules={[
              {
                required: true,
                message: 'Silakan masukkan nama Anda'
              }
            ]}
          >
            <Input className="auth-input" placeholder="Nama Lengkap" />
          </Form.Item>
          <Form.Item
            name="email"
            label="Email"
            rules={[
              {
                required: true,
                message: 'Silakan masukkan email Anda'
              },
              { type: 'email', message: 'Email tidak valid' }
            ]}
          >
            <Input type="email" placeholder="Contoh: johndee@gmail.com" />
          </Form.Item>
          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: 'Silakan masukkan password Anda'
              },
              { min: 8, message: 'Password minimal 8 karakter' }
            ]}
          >
            <Input.Password className="auth-input" placeholder="Masukkan password" />
          </Form.Item>
          <Form.Item>
            <Button
              loading={loading}
              disabled={loading}
              type="primary"
              htmlType="submit"
              block
              size="large"
            >
              Daftar
            </Button>
          </Form.Item>
        </Form>
      </Box>
      <Box className="layout-auth-info">
        <Paragraph>
          Sudah punya akun?{' '}
          <Link to="/login">
            <strong>Masuk di sini</strong>
          </Link>
        </Paragraph>
      </Box>

      <Helmet>
        <title>Daftar</title>
      </Helmet>
    </>
  );
}

export default Register;
