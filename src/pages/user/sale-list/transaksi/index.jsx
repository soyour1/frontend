import { Tabs, Skeleton } from 'antd';
import { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { Empty, Box, TransactionCard } from '@components';
import { getTransactionBuyer, getTransactionSeller } from '@redux/reducers/transaction';

const { TabPane } = Tabs;

const Transaksi = () => {
  const dispatch = useDispatch();
  const { data: transactionBuyer, loading: transactionBuyerLoading } = useSelector(
    (state) => state.transaction.transactionBuyer
  );

  const { data: transactionSeller, loading: transactionSellerLoading } = useSelector(
    (state) => state.transaction.transactionSeller
  );

  useEffect(() => {
    dispatch(getTransactionBuyer());
    dispatch(getTransactionSeller());
  }, [dispatch]);

  return (
    <>
      <Helmet>
        <title>Daftar Transaksi</title>
      </Helmet>
      <Tabs defaultActiveKey="Penjualan">
        <TabPane tab="Transaksi Penjualan" key="Penjualan">
          {transactionSellerLoading && (
            <>
              <Skeleton className="w-full h-48" active />
            </>
          )}
          {!transactionSellerLoading && transactionSeller?.length < 1 && (
            <Box>
              <Empty className="my-4 mx-auto" description="Belum ada transaksi penjualan" />
            </Box>
          )}
          {!transactionSellerLoading &&
            transactionSeller.map((transaction, i) => (
              <TransactionCard key={i} transaction={transaction} />
            ))}
        </TabPane>
        <TabPane tab="Transaksi Pembelian" key="Pembelian">
          {transactionBuyerLoading && (
            <>
              <Skeleton className="w-full h-48" active />
            </>
          )}
          {!transactionBuyerLoading && transactionBuyer?.length < 1 && (
            <Box>
              <Empty className="my-4 mx-auto" description="Belum ada transaksi pembelian" />
            </Box>
          )}
          {!transactionBuyerLoading &&
            transactionBuyer.map((transaction, i) => (
              <TransactionCard key={i} transaction={transaction} />
            ))}
        </TabPane>
      </Tabs>
    </>
  );
};

export default Transaksi;
