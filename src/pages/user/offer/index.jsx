import { Helmet } from 'react-helmet';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import {
  Box,
  UserCard,
  ProductOfferCard,
  ModalAcceptOffer,
  ModalStatus,
  BackNavigation
} from '@components';
import { getUser } from '@redux/reducers/auth';
import { getOffer, updateOffer } from '@redux/reducers/offer';

function Offer() {
  const [enableChangeStatus, setEnableStatus] = useState(true);
  const [modalAccept, setModalAccept] = useState(false);
  const [modalStatus, setModalStatus] = useState(false);
  const [status, setStatus] = useState(null);
  const { id } = useParams();
  const dispatch = useDispatch();
  const { data: offer, loading: offerLoading } = useSelector((state) => state.offer.offer);
  const { loading: rejectOfferLoading, success: successReject } = useSelector(
    (state) => state.offer.updateOfferStatus
  );
  const { loading: updateProductOfferLoading, success: successUpdateProduct } = useSelector(
    (state) => state.offer.updateProductOffer
  );

  const { data: user } = useSelector((state) => state.auth.user);

  useEffect(() => {
    if (id) {
      dispatch(getOffer(id));
    }
    dispatch(getUser());
  }, [dispatch, id]);

  useEffect(() => {
    if (offer) {
      setEnableStatus(user?.id === offer?.seller_id);
    }
  }, [offer, user]);

  useEffect(() => {
    if (successReject) {
      dispatch(getOffer(id));
      if (status === 2) {
        setModalAccept(true);
        setStatus(null);
      }
    }
  }, [successReject]); // eslint-disable-line

  useEffect(() => {
    if (successUpdateProduct) {
      dispatch(getOffer(id));
      setModalStatus(false);
    }
  }, [successUpdateProduct]);

  const rejectOffer = async () => {
    await dispatch(
      updateOffer({
        id,
        status: 0
      })
    );
  };

  const acceptOffer = async () => {
    setStatus(2);
    await dispatch(
      updateOffer({
        id,
        status: 2
      })
    );
  };

  return (
    <>
      <Box className="product container">
        <BackNavigation className="profile-back" />

        <UserCard loading={offerLoading} user={offer?.user} />
        <span className="font-medium block my-4">Produk yang ditawar</span>
        {offer && (
          <ModalAcceptOffer
            offer={offer}
            isShow={modalAccept}
            onCancel={() => setModalAccept(false)}
          />
        )}
        <ModalStatus offer={offer} isShow={modalStatus} onCancel={() => setModalStatus(false)} />
        <ProductOfferCard
          offer={offer}
          loading={offerLoading}
          showAction={enableChangeStatus}
          rejectOffer={() => rejectOffer()}
          rejectOfferLoading={rejectOfferLoading}
          acceptOffer={() => acceptOffer()}
          acceptOfferLoading={rejectOfferLoading}
          openModalStatus={() => setModalStatus(true)}
        />
      </Box>
      <Helmet>
        <title>Info Penawaran</title>
      </Helmet>
    </>
  );
}

export default Offer;
