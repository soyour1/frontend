import { Helmet } from 'react-helmet';
import { FiCamera, FiEdit3, FiSettings, FiLogOut } from 'react-icons/fi';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, Link } from 'react-router-dom';
import { Box, BackNavigation } from '@components';
import { getUser, reset } from '@redux/reducers/auth';

function Account() {
  const navigate = useNavigate();
  const [image, setImage] = useState(null);
  const dispatch = useDispatch();
  const { data: user } = useSelector((state) => state.auth.user);

  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  useEffect(() => {
    setImage(user?.image);
  }, [user]);

  const logout = () => {
    dispatch(reset());
    localStorage.removeItem('token');
    navigate('/', { replace: true });
  };

  return (
    <>
      <Box className="profile container">
        <BackNavigation className="profile-back" />
        <Box className="profile-img">
          <label htmlFor="avatar-uploader">
            {image ? (
              <img className="profile-img-avatar" src={image} alt="profile" />
            ) : (
              <Box className="profile-img-icon">
                <FiCamera size={22} />
              </Box>
            )}
          </label>
        </Box>
        <Box className="profile-menu">
          <Link to="/user/account/edit">
            <Box className="profile-menu-item">
              <FiEdit3 /> <span>Ubah Akun</span>
            </Box>
          </Link>
          <Link to="/user/account/settings">
            <Box className="profile-menu-item">
              <FiSettings /> <span>Pengaturan Akun</span>
            </Box>
          </Link>
          <Box className="profile-menu-item" onClick={() => logout()}>
            <FiLogOut /> <span>Keluar</span>
          </Box>
        </Box>
        <Box className="profile-version">Version 1.0.0</Box>
      </Box>
      <Helmet>
        <title>Akun Saya</title>
      </Helmet>
    </>
  );
}

export default Account;
