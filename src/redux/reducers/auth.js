import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '@/config';
import useToast from '@libs/toast';

export const TOKEN = localStorage.getItem('token');

export const getToken = () => {
  return localStorage.getItem('token');
};

export const login = createAsyncThunk('auth/login', async (values, { rejectWithValue }) => {
  const toast = useToast();
  try {
    const response = await axios.post(`${config.API_URL}/${config.API_VERSION}/auth/login`, values);
    if (response.status === 200) {
      localStorage.setItem('token', response.data.data.accessToken);
      toast('Login Success', 'success');
      return response;
    } else {
      rejectWithValue(response);
    }
  } catch (err) {
    toast(err?.response?.data?.error?.message, 'error');
    if (!err.response) {
      throw err;
    }
    return rejectWithValue(err.response.data);
  }
});

export const register = createAsyncThunk('auth/register', async (values, { rejectWithValue }) => {
  const toast = useToast();
  try {
    const response = await axios.post(
      `${config.API_URL}/${config.API_VERSION}/auth/register`,
      values
    );
    if (response.status === 201) {
      toast('Register Success', 'success');
      return;
    } else {
      rejectWithValue(response);
    }
  } catch (err) {
    toast(err?.response?.data?.error?.message, 'error');
    if (!err.response) {
      throw err;
    }
    return rejectWithValue(err.response.data);
  }
});

export const updateUser = createAsyncThunk('auth/profile', async (values, { rejectWithValue }) => {
  const toast = useToast();
  try {
    const response = await axios.put(
      `${config.API_URL}/${config.API_VERSION}/auth/profile`,
      values,
      {
        headers: {
          Authorization: `Bearer ${getToken()}`
        }
      }
    );
    if (response.status === 202) {
      toast('Update Success', 'success');
      return;
    } else {
      rejectWithValue(response);
    }
  } catch (err) {
    toast(err?.response?.data?.error?.message, 'error');
    if (!err.response) {
      throw err;
    }
    return rejectWithValue(err.response.data);
  }
});

export const updatePassword = createAsyncThunk(
  'auth/password',
  async (values, { rejectWithValue }) => {
    const toast = useToast();
    try {
      const response = await axios.put(
        `${config.API_URL}/${config.API_VERSION}/auth/password`,
        values,
        {
          headers: {
            Authorization: `Bearer ${getToken()}`
          }
        }
      );
      if (response.status === 202) {
        toast('Update Success', 'success');
        return;
      } else {
        rejectWithValue(response);
      }
    } catch (err) {
      toast(err?.response?.data?.error?.message, 'error');
      if (!err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data);
    }
  }
);

export const getUser = createAsyncThunk('auth/whoami', async () => {
  try {
    const response = await axios.get(`${config.API_URL}/${config.API_VERSION}/auth/whoami`, {
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
    if (response.status === 200) {
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const confirm = createAsyncThunk('auth/confirm', async (values, { rejectWithValue }) => {
  const toast = useToast();
  try {
    const response = await axios.post(
      `${config.API_URL}/${config.API_VERSION}/auth/confirm`,
      values
    );
    if (response.status === 201) {
      toast(response.data.message, 'success');
      return response;
    } else {
      rejectWithValue(response);
    }
  } catch (err) {
    toast(err?.response?.data?.error?.message, 'error');
    if (!err.response) {
      throw err;
    }
    return rejectWithValue(err.response.data);
  }
});

export const resend = createAsyncThunk('auth/resend', async (values, { rejectWithValue }) => {
  const toast = useToast();
  try {
    const response = await axios.post(
      `${config.API_URL}/${config.API_VERSION}/auth/resendEmail`,
      values
    );
    if (response.status === 201) {
      toast('Resend Email Verification Success', 'success');
      return response;
    } else {
      rejectWithValue(response);
    }
  } catch (err) {
    toast(err?.response?.data?.error?.message, 'error');
    if (!err.response) {
      throw err;
    }
    return rejectWithValue(err.response.data);
  }
});

const initialState = {
  login: {
    token: getToken() || null,
    loading: false,
    error: false,
    errorMessage: null,
    success: getToken() ? true : false
  },
  register: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false
  },
  user: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false,
    data: []
  },
  updateUser: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false
  },
  updatePassword: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false
  },
  confirm: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false
  },
  resend: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false
  }
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: {
    // LOGIN
    [login.pending]: (state) => {
      state.login.loading = true;
    },
    [login.fulfilled]: (state, action) => {
      state.login.token = action.payload.data.data.accessToken;
      state.login.success = true;
      state.login.error = false;
      state.login.errorMessage = null;
      state.login.loading = false;
    },
    [login.rejected]: (state, action) => {
      state.login.success = false;
      state.login.error = action.error.message;
      state.login.errorMessage = action.payload.message;
      state.login.loading = false;
    },
    // REGISTER
    [register.pending]: (state) => {
      state.register.loading = true;
    },
    [register.fulfilled]: (state, action) => {
      state.register.success = true;
      state.register.error = false;
      state.register.errorMessage = null;
      state.register.loading = false;
    },
    [register.rejected]: (state, action) => {
      state.register.success = false;
      state.register.error = action.error.message;
      state.register.errorMessage = action.payload.message;
      state.register.loading = false;
    },
    // GET USER
    [getUser.pending]: (state) => {
      state.user.loading = true;
    },
    [getUser.fulfilled]: (state, action) => {
      state.user.data = action.payload.data.data;
      state.user.success = true;
      state.user.error = false;
      state.user.errorMessage = null;
      state.user.loading = false;
    },
    [getUser.rejected]: (state, action) => {
      state.user.success = false;
      state.user.error = action.error.message;
      state.user.errorMessage = action.payload.message;
      state.user.loading = false;
    },
    // UPDATE PROFILE
    [updateUser.pending]: (state) => {
      state.updateUser.loading = true;
    },
    [updateUser.fulfilled]: (state, action) => {
      state.updateUser.success = true;
      state.updateUser.error = false;
      state.updateUser.errorMessage = null;
      state.updateUser.loading = false;
    },
    [updateUser.rejected]: (state, action) => {
      state.updateUser.success = false;
      state.updateUser.error = action.error.message;
      state.updateUser.errorMessage = action.payload.message;
      state.updateUser.loading = false;
    },
    // UPDATE PASSWORD
    [updatePassword.pending]: (state) => {
      state.updatePassword.loading = true;
    },
    [updatePassword.fulfilled]: (state, action) => {
      state.updatePassword.success = true;
      state.updatePassword.error = false;
      state.updatePassword.errorMessage = null;
      state.updatePassword.loading = false;
    },
    [updatePassword.rejected]: (state, action) => {
      state.updatePassword.success = false;
      state.updatePassword.error = action.error.message;
      state.updatePassword.errorMessage = action.payload.message;
      state.updatePassword.loading = false;
    },
    // CONFIRM
    [confirm.pending]: (state) => {
      state.confirm.loading = true;
    },
    [confirm.fulfilled]: (state, action) => {
      state.confirm.success = true;
      state.confirm.error = false;
      state.confirm.errorMessage = null;
      state.confirm.loading = false;
    },
    [confirm.rejected]: (state, action) => {
      state.confirm.success = false;
      state.confirm.error = action.error.message;
      state.confirm.errorMessage = action.payload.message;
      state.confirm.loading = false;
    },
    // RESEND
    [resend.pending]: (state) => {
      state.resend.loading = true;
    },
    [resend.fulfilled]: (state, action) => {
      state.resend.success = true;
      state.resend.error = false;
      state.resend.errorMessage = null;
      state.resend.loading = false;
    },
    [resend.rejected]: (state, action) => {
      state.resend.success = false;
      state.resend.error = action.error.message;
      state.resend.errorMessage = action.payload.message;
      state.resend.loading = false;
    }
  }
});

export const { reset } = authSlice.actions;

export default authSlice.reducer;
