import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import config from '@/config';
import useToast from '@libs/toast';

export const forgetPassword = createAsyncThunk(
  'password/forget',
  async (values, { rejectWithValue }) => {
    const toast = useToast();
    try {
      const response = await axios.post(
        `${config.API_URL}/${config.API_VERSION}/auth/password/forget`,
        values
      );
      if (response.status === 201) {
        toast('Send Email Verification Success', 'success');
        return response;
      } else {
        rejectWithValue(response);
      }
    } catch (err) {
      toast(err?.response?.data?.error?.message, 'error');
      if (!err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data);
    }
  }
);

export const resetPassword = createAsyncThunk(
  'password/reset',
  async (values, { rejectWithValue }) => {
    const toast = useToast();
    try {
      const response = await axios.post(
        `${config.API_URL}/${config.API_VERSION}/auth/password/reset`,
        values
      );
      if (response.status === 201) {
        toast('Success reset password', 'success');
        return response;
      } else {
        rejectWithValue(response);
      }
    } catch (err) {
      toast(err?.response?.data?.error?.message, 'error');
      if (!err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data);
    }
  }
);

const initialState = {
  forget: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false
  },
  reset: {
    loading: false,
    error: false,
    errorMessage: null,
    success: false
  }
};

export const passwordSlice = createSlice({
  name: 'password',
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: {
    // FORGET
    [forgetPassword.pending]: (state) => {
      state.forget.loading = true;
    },
    [forgetPassword.fulfilled]: (state, action) => {
      state.forget.success = true;
      state.forget.error = false;
      state.forget.errorMessage = null;
      state.forget.loading = false;
    },
    [forgetPassword.rejected]: (state, action) => {
      state.forget.success = false;
      state.forget.error = action.error.message;
      state.forget.errorMessage = action.payload.message;
      state.forget.loading = false;
    },
    // RESET
    [resetPassword.pending]: (state) => {
      state.reset.loading = true;
    },
    [resetPassword.fulfilled]: (state, action) => {
      state.reset.success = true;
      state.reset.error = false;
      state.reset.errorMessage = null;
      state.reset.loading = false;
    },
    [resetPassword.rejected]: (state, action) => {
      state.reset.success = false;
      state.reset.error = action.error.message;
      state.reset.errorMessage = action.payload.message;
      state.reset.loading = false;
    }
  }
});

export const { reset } = passwordSlice.actions;

export default passwordSlice.reducer;
