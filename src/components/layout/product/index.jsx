import { Outlet } from 'react-router-dom';
import { Box, Footer, Header } from '@components';

const ProductLayout = () => {
  return (
    <Box className="layout-general">
      <Header isHome isProduct />
      <Box className="layout-content">
        <Outlet />
      </Box>
      <Footer />
    </Box>
  );
};

export default ProductLayout;
