import { Modal } from 'antd';
import React from 'react';
const Box = ({
  Title,
  ButtonText,
  Icon,
  ModalText,
  ModalIcon,
  button,
  isShow,
  onCancel,
  children,
  ...props
}) => {
  return (
    <>
      <Modal visible={isShow} {...props} width={360} onCancel={onCancel}>
        <h1>{Title || 'Judul Modal'}</h1>
        {children}
      </Modal>
    </>
  );
};

export default Box;
