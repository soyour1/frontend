import cx from 'classnames';

const Skeleton = ({ className, ...props }) => {
  return <span className={cx('skeleton-loading', className)} {...props}></span>;
};

export default Skeleton;
