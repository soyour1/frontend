import { FiSearch } from 'react-icons/fi';
import cx from 'classnames';
import { Box } from '@components';

const Category = ({ category, isActive, ...res }) => {
  return (
    <Box className={cx('category', isActive && 'category-active')} {...res}>
      <Box className="category-icon">
        <FiSearch size={24} />{' '}
      </Box>
      <Box>{category?.name}</Box>
    </Box>
  );
};
export default Category;
