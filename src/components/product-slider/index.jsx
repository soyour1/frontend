import SwiperCore, { Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Skeleton } from 'antd';
import { useRef } from 'react';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

const ProductSlider = ({ loading = false, product }) => {
  SwiperCore.use([Navigation]);
  const sliderRef = useRef();

  return loading ? (
    <Skeleton.Button className="skeleton-product-slider mb-6" active />
  ) : (
    <div className="product-slider">
      <Swiper
        ref={sliderRef}
        pagination={true}
        navigation={true}
        modules={[Navigation, Pagination]}
      >
        {product?.images?.map((img) => (
          <SwiperSlide>
            <img alt={img?.url} src={img?.url} />
          </SwiperSlide>
        ))}
        {product?.images?.length < 1 && (
          <SwiperSlide>
            <img alt="slider" src={'https://picsum.photos/600/600'} />
          </SwiperSlide>
        )}
      </Swiper>
    </div>
  );
};

export default ProductSlider;
