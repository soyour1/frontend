import { Input, Button, Form, Drawer } from 'antd';
import { FiLogIn, FiSearch, FiMenu } from 'react-icons/fi';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import cx from 'classnames';

import { getProductsByName } from '@redux/reducers/product';
import { ReactComponent as Logo } from '@assets/svg/logo-auth.svg';
import { Box, LoggedInIcon, BackNavigation } from '@components';

const Header = ({ type = 'general', isHome = false, isSale = false, isProduct = false }) => {
  const isLogin = localStorage.getItem('token') !== null;
  const [title, setTitle] = useState(null);
  const [visible, setVisible] = useState(false);
  const [form] = Form.useForm();
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onSubmit = (values) => {
    if (location.pathname !== '/') {
      navigate(`/?name=${values?.name}`, { replace: true });
    } else {
      dispatch(
        getProductsByName({
          name: values?.name,
          page: 1
        })
      );
    }
  };

  useEffect(() => {
    form.resetFields();
  }, [location.pathname]); // eslint-disable-line

  return (
    <>
      <Box className={cx('header', isHome ? 'header-home' : '')}>
        <Helmet onChangeClientState={(newState) => setTitle(newState.title)} />
        <Box className="container">
          {type === 'general' && (
            <Box className="header-wrapper">
              <Box className="header-wrapper-left">
                <Box className="header-logo">
                  <Link to="/">
                    <Logo />
                  </Link>
                </Box>
                <Button
                  type="primary"
                  size="large"
                  ghost
                  className="header-btn"
                  onClick={() => setVisible(true)}
                >
                  <FiMenu size={24} />
                </Button>
                <Box className={cx(isSale ? 'block md:hidden' : 'hidden', 'font-bold text-xl')}>
                  {title}
                </Box>
                <Form
                  form={form}
                  layout="vertical"
                  onFinish={onSubmit}
                  autoComplete="off"
                  className={cx((isSale || isProduct) && 'hidden md:block')}
                >
                  <Form.Item name="name">
                    <Input
                      className="header-search"
                      placeholder="Cari di sini ..."
                      suffix={<FiSearch size={24} />}
                    />
                  </Form.Item>
                </Form>
              </Box>
              {isLogin ? (
                <Box className="header-auth">
                  <LoggedInIcon />
                </Box>
              ) : (
                <Link to="/login" className="header-auth">
                  <Button type="primary" size="large">
                    <FiLogIn className="inline-block mr-2" size={20} /> Masuk
                  </Button>
                </Link>
              )}
            </Box>
          )}
          {type === 'app' && (
            <Box className="relative ">
              <Box className="text-base absolute left-0 right-0 top-0 bottom-0 flex items-center justify-center">
                {title}
              </Box>
              <Box className="min-h-[52px] flex items-center relative z-10">
                <Box className="header-logo">
                  <Link to="/">
                    <Logo />
                  </Link>
                </Box>
                <BackNavigation className="block md:hidden cursor-pointer" />
              </Box>
            </Box>
          )}
        </Box>
      </Box>
      <Drawer
        className="header-drawer"
        title={
          <Link className="font-black text-black" to="/">
            Secondhand
          </Link>
        }
        placement="left"
        closable={true}
        onClose={() => setVisible(false)}
        visible={visible}
        width={240}
      >
        {isLogin ? (
          <Box>
            <LoggedInIcon />
          </Box>
        ) : (
          <Link to="/login">
            <Button type="primary" size="large">
              <FiLogIn className="inline-block mr-2" size={20} /> Masuk
            </Button>
          </Link>
        )}
      </Drawer>
    </>
  );
};

export default Header;
