import { ReactComponent as Back } from '@assets/svg/back.svg';
import { useNavigate } from 'react-router-dom';

const BackNavigation = ({ ...res }) => {
  const navigate = useNavigate();
  return (
    <Back className="cursor-pointer" {...res} onClick={() => navigate(-1, { replace: true })} />
  );
};

export default BackNavigation;
