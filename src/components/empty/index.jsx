import { Box } from '@components';
import { ReactComponent as EmptyImg } from '@assets/svg/Empty-Product.svg';

const EmptyProduct = ({ description, ...rest }) => {
  return (
    <Box className="empty-product" {...rest}>
      <EmptyImg className="empty-product-image" />
      <Box className="empty-product-text">{description}</Box>
    </Box>
  );
};

export default EmptyProduct;
