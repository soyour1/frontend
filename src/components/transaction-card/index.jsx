import { Card, Typography } from 'antd';
import dateFormat from 'dateformat';
import { formatRupiah } from '@libs/number';
const { Text, Title } = Typography;

function TransactionCard({ transaction }) {
  return (
    <div className="mb-4">
      <Card>
        <Title level={5}>{transaction?.invoice_id}</Title>
        <Text>
          {transaction?.product?.name} ({formatRupiah(transaction?.price)})
        </Text>
        <br />
        <Text type="secondary">{dateFormat(transaction?.created_at, 'dd mmm, HH:MM')}</Text>
      </Card>
    </div>
  );
}

export default TransactionCard;
