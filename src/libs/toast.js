import { toast } from 'react-toastify';

const useToast = () => {
  return (message, severity = 'default', autoClose = null) => {
    const availableSeverity = ['success', 'error', 'warn', 'info', 'default'];
    if (!availableSeverity.includes(severity)) {
      throw Error('Toast severity is not supported.');
    }

    const notify = severity === 'default' ? toast : toast[severity];

    notify(message, {
      position: toast.POSITION.TOP_CENTER,
      theme: 'colored',
      className: '',
      autoClose: autoClose,
      icon: false
    });
  };
};

export default useToast;
